<div class="card">
    <div class="card-header">Header</div>
    <div class="card-body">
        <nav class="navbar bg-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/admin/championship">Championship</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/apis">APIs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/fetchers">Fetchers</a>
                </li>
            </ul>
        </nav>
    </div>
</div>