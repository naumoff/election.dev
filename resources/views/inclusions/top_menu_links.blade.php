<li class="nav-item">
    <a class="nav-link" href="/play">Play</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="/score">Score</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="/champions">Champions</a>
</li>

@if(Auth::check() && Auth::user()->isAdmin)
    <li class="nav-item">
        <a class="nav-link" href="/home">Administration</a>
    </li>
@endif
