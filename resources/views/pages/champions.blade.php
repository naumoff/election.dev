@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table table-striped" >
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Score</th>
                        <th>Won</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($champions AS $champion)
                            <tr>
                                <td>
                                    <img
                                        src="{{$champion->image_url}}"
                                        class="rounded-circle"
                                        alt="Cinque Terre"
                                        width="80"
                                        height="80"
                                    >
                                </td>
                                <td>{{$champion->fetcher}}</td>
                                <td>{{$champion->field_1}}</td>
                                <td>{{$champion->score}}</td>
                                <td>{{$champion->created_at->diffForHumans()}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $champions->links() }}
                <div>

                </div>
            </div>
        </div>
    </div>
@endsection