@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-light">
                     <strong>Championship ended!</strong>
                    Please contact system administrator to initiate new championship!
                </div>
            </div>
        </div>
    </div>
@endsection