@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table table-striped" >
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Score</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($scoreResults as $item)
                        <tr>
                            <td>{{$item['type']}}</td>
                            <td>{{$item['column_1']}}</td>
                            <td>{{$item['score']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div>
                    {{ $scoreResults->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection