@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($players AS $playerKey=>$playerFields)
                <div class="card">
                    @if ($loop->first)
                        <div class="card-header">Player One</div>
                    @else
                        <div class="card-header">Player Two</div>
                    @endif
                    <br>
                        <img class="rounded-circle card-img-top"
                             src="{{$playerFields['6_image_url']}}"
                             alt="noimage"
                             width="90"
                             height="90"
                        >
                    <div class="card-body">
                        @foreach( $playerFields AS $field=>$value)
                            @if($loop->index <=3)
                                <b>{{$field}}</b> : {{$value}}<br>
                            @endif
                        @endforeach
                        @if ($loop->first)
                            <p style="color: red">Player Score: <b>{{$score1}}</b></p>
                        @else
                            <p style="color: red">Player Score: <b>{{$score2}}</b></p>
                        @endif
                    </div>
                    <div class="card-footer">
                        @if ($loop->first)
                            <form method="POST" action="{{route('endgame')}}">
                                {{csrf_field()}}
                                <input type="text" value="{{$playerOneKey}}" name="winner" hidden>
                                <input type="text" value="{{$playerTwoKey}}" name="looser" hidden>
                                <button type="submit" class="btn btn-primary btn-block play">Vote</button>
                            </form>
                        @else
                            <form method="POST" action="{{route('endgame')}}">
                                {{csrf_field()}}
                                <input type="text" value="{{$playerOneKey}}" name="looser" hidden>
                                <input type="text" value="{{$playerTwoKey}}" name="winner" hidden>
                                <button type="submit" class="btn btn-primary btn-block play">
                                    Vote
                                </button>
                            </form>
                        @endif

                    </div>
                </div>
                @endforeach
             </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.play').on('click', function(){
                $('.card').hide();
            })
        });
    </script>
@endsection