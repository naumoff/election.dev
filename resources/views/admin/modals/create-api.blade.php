<!-- The Modal -->
<div class="modal fade" id="newApi">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="POST" action="{{route('apis.store')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="base_uri">Base Uri:</label>
                        <input type="url" class="form-control" id="base_uri" placeholder="Base Uri" name="base_uri">
                    </div>
                    <div class="form-group">
                        <label for="request_limit">Requests Limit:</label>
                        <input type="number" min="1" max="200" class="form-control" id="request_limit" placeholder="Request Limit" name="request_limit" required>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>