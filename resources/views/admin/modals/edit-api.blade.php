<!-- The Modal -->
<div class="modal fade" id="Api{{$api->id}}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="POST" action="{{route('apis.update', ['id'=>$api->id])}}">
                    {{ csrf_field() }}
                    @method('PATCH')
                    <input type="number" name="id" value="{{$api->id}}" hidden>
                    <div class="form-group">
                        <label for="base_uri">Base Uri:</label>
                        <input type="url" value="{{$api->base_uri}}" class="form-control" id="base_uri" placeholder="Base Uri" name="base_uri">
                    </div>
                    <div class="form-group">
                        <label for="request_limit">Requests Limit:</label>
                        <input type="number" value="{{$api->request_limit}}" min="1" max="200" class="form-control" id="request_limit" placeholder="Request Limit" name="request_limit" required>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>