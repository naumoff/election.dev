@extends('admin.admin')

@section('body')
    <div class="card">
        <div class="card-header">Start Championship</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <p>Matches left: {{$matchesLeft}}</p>
            <a href="#" class="btn btn-primary btn-block btn-sm gen" role="button">Generate New Championship</a>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.gen').on('click', function(){
                $('.gen').hide();
                $.post
                (
                    '{{route('generate')}}',
                    {
                        "_token": "{{ csrf_token() }}",
                    },
                    function(data)
                    {
                        alert(data);
                    }
                );
            })
        });
    </script>
@endsection