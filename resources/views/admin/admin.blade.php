@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                @include('inclusions.vertical_admin_links')
            </div>
            <div class="col-md-9">
                @yield('body')
            </div>
        </div>
    </div>
@endsection