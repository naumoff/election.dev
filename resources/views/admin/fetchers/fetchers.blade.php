@extends('admin.admin')

@section('body')
    @if(count($fetchers)>0)
    <div class="card">
        <div class="card-header">Fetchers</div>
        <div class="card-body">
            <table class="table table-striped table-responsive" >
                <thead>
                <tr>
                    <th>Base Uri</th>
                    <th>Endpoint</th>
                    <th>Fetcher Name</th>
                    <th>Serving Class</th>
                    <th>Details</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($fetchers AS $fetcher)
                    <tr>
                        <td>{{$fetcher->api->base_uri}}</td>
                        <td>{{$fetcher->endpoint}}</td>
                        <td>{{$fetcher->name}}</td>
                        <td>{{$fetcher->fetcher_type->fetcher_class}}</td>
                        <td>
                            <a href="/admin/fetchers/{{$fetcher->id}}" class="btn btn-info btn-sm" role="button">Details</a>
                        </td>
                        <td>
                            <a href="#"
                               class="btn btn-danger btn-sm del"
                               role="button"
                               id="{{route('fetchers.destroy',['id'=>$fetcher->id])}}">
                                Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
    <a href="{{route('fetchers.create')}}" class="btn btn-success btn-block btn-sm" role="button">Create New EndPoint</a>
    <script>
        $(document).ready(function(){
            $('.del').on('click', function(){
                var url = $(this).attr('id');
                $.post
                (
                    url,
                    {
                        "_token": "{{ csrf_token() }}",
                        "_method": "DELETE"
                    },
                    function(data)
                    {
                        if(data === 'yes'){
                            location.reload();
                        }
                    }
                );
            })
        });
    </script>
@endsection