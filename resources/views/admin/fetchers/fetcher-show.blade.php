@extends('admin.admin')

@section('body')
    <div class="card">
        <div class="card-header">Fetcher </div>
        <div class="card-body">
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="api">Api:</label>
                        <input type="text" class="form-control" id="api" value="{{$fetcher->api->base_uri}}" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="endpoint">Endpoint:</label>
                        <input type="text" class="form-control" id="endpoint" value="{{$fetcher->endpoint}}" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="api_request_limit">Api requests qty limit:</label>
                        <input type="text" class="form-control" id="api_request_limit" value="{{$fetcher->api->request_limit}}" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="endpoint_request_limit">Endpoint requests qty limit:</label>
                        <input type="text" class="form-control" id="endpoint_request_limit" value="{{$fetcher->request_limit}}" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="fetcher_type_name">Fetcher Type Name:</label>
                        <input type="text" class="form-control" id="fetcher_type_name" value="{{$fetcher->fetcher_type->name}}" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="fetcher_type_class_name">Fetcher Class Name:</label>
                        <input type="text" class="form-control" id="fetcher_type_class_name" value="{{$fetcher->fetcher_type->fetcher_class}}" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="fetcher_type_description">Fetcher Type Description:</label>
                        <textarea class="form-control" id="fetcher_type_description"readonly>{{$fetcher->fetcher_type->description}}</textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="all_results_field">Results field (for one and two steps fetchers):</label>
                        <input type="text" class="form-control" id="all_results_field" value="{{$fetcher->all_results_field}}" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="all_results_subfield">Results subfield (for two steps fetchers ONLY):</label>
                        <input type="text" class="form-control" id="all_results_subfield" value="{{$fetcher->all_results_subfield}}" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="field_1">Field 1:</label>
                        <input type="text" class="form-control" id="field_1" value="{{$fetcher->field_1}}" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="field_2">Field 2:</label>
                        <input type="text" class="form-control" id="field_2" value="{{$fetcher->field_2}}" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="field_3">Field 3:</label>
                        <input type="text" class="form-control" id="field_3" value="{{$fetcher->field_3}}" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="field_4">Field 4:</label>
                        <input type="text" class="form-control" id="field_4" value="{{$fetcher->field_4}}" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="image_url">Image Url:</label>
                        <input type="text" class="form-control" id="image_url" value="{{$fetcher->image_url}}" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="entity_url">Entity Url (for one step fetchers ONLY):</label>
                        <input type="text" class="form-control" id="entity_url" value="{{$fetcher->entity_url}}" readonly>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection