@extends('admin.admin')

@section('body')
    <div class="card">
        <div class="card-header">Fetcher </div>
        <div class="card-body">
            @include('messages.messages')
            @include('messages.errors')
            <form method="POST" action="{{route('fetchers.store')}}">
                {{csrf_field()}}
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <label for="api_id">
                                Select base uri (select one):
                            </label>
                            <select class="form-control" id="api_id" name="api_id">
                                <option>Selection required!</option>
                                @foreach($apis AS $api)
                                    <option value="{{$api->id}}">{{$api->base_uri}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <label for="fetcher_type_id">
                                Select fetcher class (select one):
                            </label>
                            <select class="form-control" id="fetcher_type_id" name="fetcher_type_id">
                                <option>Selection required!</option>
                                @foreach($fetcherTypes AS $fetcherType)
                                    <option value="{{$fetcherType->id}}">{{$fetcherType->fetcher_class}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter unique name" name="name" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="endpoint">Endpoint:</label>
                        <input type="text" class="form-control" id="endpoint" placeholder="Enter endpoint" name="endpoint" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="request_limit">Request Limit:</label>
                        <input type="number" min="1" max="100" class="form-control" id="request_limit" placeholder="Enter request limit" name="request_limit" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="next_link">Next Link:</label>
                        <input type="text" class="form-control" id="next_link" placeholder="Enter next link" name="next_link" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="all_results_field">All Results Field:</label>
                        <input type="text" class="form-control" id="all_results_field" placeholder="Enter All Results Field" name="all_results_field" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="all_results_subfield">All Results Subfield:</label>
                        <input type="text" class="form-control" id="all_results_subfield" placeholder="Enter All Results Subfield (two steps only)" name="all_results_subfield">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="field_1">Field 1:</label>
                        <input type="text" class="form-control" id="field_1" placeholder="Enter field_1 name" name="field_1" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="field_2">Field 2:</label>
                        <input type="text" class="form-control" id="field_2" placeholder="Enter field_2 name" name="field_2" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="field_3">Field 3:</label>
                        <input type="text" class="form-control" id="field_3" placeholder="Enter field_3 name" name="field_3" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="field_4">Field 4:</label>
                        <input type="text" class="form-control" id="field_4" placeholder="Enter field_4 name" name="field_4" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="image_url">Image Url:</label>
                        <input type="url" class="form-control" id="image_url" placeholder="Enter image url name" name="image_url">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="fentity_url">Entity Url:</label>
                        <input type="url" class="form-control" id="entity_url" placeholder="Enter entity url name (one step only)" name="entity_url">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection