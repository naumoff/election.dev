@extends('admin.admin')

@section('body')
    <div class="card">
        <div class="card-header">Fetchers</div>
        @if(count($apis)>0)
        <div class="card-body">
            @include('messages.messages')
            @include('messages.errors')
            <table class="table table-striped table-responsive" >
                <thead>
                <tr>
                    <th>Base Uri</th>
                    <th>Request Limit</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($apis AS $api)
                        <tr>
                            <td>{{$api->base_uri}}</td>
                            <td>{{$api->request_limit}}</td>
                            <td>
                                <button type="button" class="btn btn-info btn-block btn-sm" data-toggle="modal" data-target="#Api{{$api->id}}">
                                    Edit Api
                                </button>
                                @include('admin.modals.edit-api')
                            </td>
                            <td>
                                <a href="#"
                                   class="btn btn-danger btn-sm del"
                                   role="button"
                                   id="{{route('apis.destroy',['id'=>$api->id])}}">
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endif
        <button type="button" class="btn btn-success btn-block btn-sm" data-toggle="modal" data-target="#newApi">
            Create New Api
        </button>
        @include('admin.modals.create-api')
    </div>
    <script>
        $(document).ready(function(){
            $('.del').on('click', function(){
                var url = $(this).attr('id');
                $.post
                (
                    url,
                    {
                        "_token": "{{ csrf_token() }}",
                        "_method": "DELETE"
                    },
                    function(data)
                    {
                        if(data === 'yes'){
                            location.reload();
                        }
                    }
                );
            })
        });
    </script>
@endsection