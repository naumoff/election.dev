<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <style>
        body {
            padding-bottom: 10px;
        }
        .level {
            display:flex;
            align-items: center;
        }
        .flex{
            flex: 1;
        }
    </style>
</head>
<body>
<div id="app">
    <div class="container-fluid">
        <h1 style="text-align: center">Entities War</h1>
    </div>
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel sticky-top">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    @include('inclusions.top_menu_links')
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @include('inclusions.auth_top_menu_links')
                </ul>
            </div>
        </div>
    </nav>
    <br>

    <main class="py-4">
        @yield('content')
    </main>

</div>
</body>
</html>