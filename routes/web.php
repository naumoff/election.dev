<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/emulate', function(){
    for($a=0; $a<30; $a++){

        $players = \App\Services\Facades\MatchPlayer::startMatchAndGetPlayersKeys();
        $player1 = $players[0];
        dump($player1);
        $player2 = $players[1];
        dump($player2);

        \App\Services\Facades\MatchPlayer::lost($player1);
        \App\Services\Facades\MatchPlayer::won($player2);
    }
});


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/play', 'PlayGameController@play');

Route::post('/endgame', 'PlayGameController@endgame')->name('endgame');

Route::get('/score', 'PlayGameController@score');

Route::get('/champions', 'PlayGameController@champions')->name('champions');

Route::group(['middleware'=>'admin', 'prefix'=>'admin'], function() {

    Route::get('/championship', 'SetGameController@startChampionship');
    Route::post('/generate-championship', 'SetGameController@generateChampionship')->name('generate');

    Route::resource('/apis', 'ApiController', ['except' => ['create', 'show', 'edit']]);
    Route::resource('/fetchers', 'FetcherController', ['except'=>['edit','update']]);
});
