<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fetcher extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'fetchers';
    protected $fillable = [
        'api_id','fetcher_type_id', 'name','endpoint','request_limit','next_link',
        'all_results_field','all_results_subfield','field_1','field_2','field_3','field_4',
        'image_url','entity_url'
    ];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    public function api()
    {
        return $this->belongsTo(Api::class);
    }

    public function fetcher_type()
    {
        return $this->belongsTo(FetcherType::class);
    }
    #endregion
}
