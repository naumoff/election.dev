<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FetcherType extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'fetcher_types';
    protected $fillable = [

    ];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    public function fetchers()
    {
        return $this->hasMany(Fetcher::class);
    }
    #endregion
}
