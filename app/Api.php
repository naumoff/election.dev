<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'apis';
    protected $fillable = ['base_uri','request_limit'];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    public function fetchers()
    {
        return $this->hasMany(Fetcher::class);
    }
    #endregion
}
