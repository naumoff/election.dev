<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'roles';
    protected $fillable = [];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    public function users()
    {
        return $this->hasMany(User::class);
    }
    #endregion
}
