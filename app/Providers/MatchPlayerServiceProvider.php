<?php

namespace App\Providers;

use App\Services\Matches\OneWinnerMatch;
use Illuminate\Support\ServiceProvider;

class MatchPlayerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('MatchPlayer', function(){
            return new OneWinnerMatch();
        });
    }
}
