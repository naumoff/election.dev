<?php

namespace App\Providers;

use App\Services\CacheRecorders\RedisCacheRecorder;
use Illuminate\Support\ServiceProvider;

class CacheRecorderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('CacheRec', function(){
            return new RedisCacheRecorder();
        });
    }
}
