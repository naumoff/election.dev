<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/8/2018
 * Time: 11:25 AM
 */

namespace App\Services\ApiFetchers\Fetchers;

use App\Fetcher;
use App\Services\Facades\LogRec;
use GuzzleHttp\Psr7\Request;
use App\Services\Facades\CacheRec;

final class TwoStepFetcher extends ApiFetcher
{
    private $retrievedEntityLinks;

    public function __construct(Fetcher $fetcher)
    {
        parent::__construct($fetcher);
    }

    #region MAIN METHODS
    public function handle()
    {
        for($requestQty = 0; $requestQty < $this->fetcherRequestLimit; $requestQty++)
        {
            if($this->requestQtyLimitsReached()){
                LogRec::debug([
                    'process'=>'fetching data from remote api',
                    'message'=>"requests limit exceeded for {$this->fetcher->name}"
                ]);
                break;
            }

            $this->incrementRequestQty();

            //put logic here
            if($requestQty === 0){
                [$this->retrievedEntityLinks, $this->nextPageLink] = $this->getFirstRequestResults();
                $this->fetchEntities($this->retrievedEntityLinks);
            }else{
                if($this->nextPageLink){
                    [$this->retrievedEntityLinks, $this->nextPageLink] = $this->getNextRequestResults($this->nextPageLink);
                    $this->fetchEntities($this->retrievedEntityLinks);
                }
            }

            if(!$this->nextPageLink){
                LogRec::debug([
                    'process'=>"fetching data from remote api",
                    'message'=>"pagination for fetcher {$this->fetcher->name} reached end"
                ]);
                break;
            }
        }
        CacheRec::savePlayers($this->retrievedData, $this->fetcher);
    }
    #endregion

    #region SERVICE METHODS
    private function fetchEntities($linksArray)
    {
        $headers = []; //@todo finish later

        foreach ($linksArray AS $link){

            if($this->requestQtyLimitsReached()){
                LogRec::debug([
                    'process'=>'fetching data from TwoStepFetcher remote api',
                    'message'=>"requests limit exceeded for {$this->fetcher->name}"
                ]);
                break;
            }

            $this->incrementRequestQty();

            $field = $this->fetcher->all_results_subfield;
            $newEndPoint = str_replace($this->fetcher->api->base_uri, '', $link->$field);

            $request = new Request('GET', $newEndPoint, $headers);

            $promise = $this->client->sendAsync($request);

            $response = $promise->wait();

            $result = $this->cleanFetchedResults( (array) \GuzzleHttp\json_decode($response->getBody()), $link->$field);

            $this->retrievedData = array_merge($this->retrievedData, $result);
        }
    }

    private function cleanFetchedResults($rawData, $link)
    {
        $cleanData[] = [
            '1_'.$this->fetcher->field_1 => $rawData[$this->fetcher->field_1],
            '2_'.$this->fetcher->field_2 => $rawData[$this->fetcher->field_2],
            '3_'.$this->fetcher->field_3 => $rawData[$this->fetcher->field_3],
            '4_'.$this->fetcher->field_4 => $rawData[$this->fetcher->field_4],
            '5_entity_url' => $link
        ];

        return $cleanData;
    }
    #endregion
}