<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/7/2018
 * Time: 2:08 PM
 */

namespace App\Services\ApiFetchers\Fetchers;


use App\Fetcher;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Psr7\Request;

abstract class ApiFetcher
{
    protected $fetcher;

    protected $client;

    protected $retrievedData = [];

    protected $apiRequestLimit;

    protected $fetcherRequestLimit;

    protected $nextPageLink;

    public function __construct(Fetcher $fetcher)
    {
        $this->fetcher = $fetcher;
        $this->makeClient();
        $this->setApiRequestLimit();
        $this->setFetcherRequestLimit();
    }

    abstract public function handle();

    #region SERVICE METHODS
    private function makeClient()
    {
            $this->client = new Client([
                'base_uri'=>$this->fetcher->api->base_uri,
                'timeout'=>900
            ]);
    }

    private function setApiRequestLimit()
    {
        $this->apiRequestLimit = $this->fetcher->api->request_limit;
    }

    private function setFetcherRequestLimit()
    {
        $this->fetcherRequestLimit = $this->fetcher->request_limit;
    }

    #endregion

    #region INHERITED METHODS
    protected function incrementRequestQty()
    {
        // api increment
        if(Cache::has('api:'.$this->fetcher->api->id)){
            Cache::increment('api:'.$this->fetcher->api->id);
        }else{
            Cache::rememberForever('api:'.$this->fetcher->api->id, function(){
                return 1;
            });
        }

        // fetcher increment
        if(Cache::has('fetcher:'.$this->fetcher->id)){
            Cache::increment('fetcher:'.$this->fetcher->id);
        }else{
            Cache::rememberForever('fetcher:'.$this->fetcher->id, function(){
                return 1;
            });
        }
    }

    protected function requestQtyLimitsReached()
    {
        if($this->apiRequestLimit <= Cache::get('api:'.$this->fetcher->api->id)){
            return true;
        }

        if($this->fetcherRequestLimit <= Cache::get('fetcher:'.$this->fetcher->id)){
            return true;
        }
        return false;
    }

    protected function getFirstRequestResults(): array
    {
        return $this->getResults();
    }

    protected function getNextRequestResults($nextPageLink): array
    {
        $newEndPoint = str_replace($this->fetcher->api->base_uri, '', $nextPageLink);
        if($newEndPoint){
            return $this->getResults($newEndPoint);
        }
    }

    protected function getResults($endpoint = null): array
    {
        $headers = []; //@todo add later

        if($endpoint === null){
            $request = new Request('GET', $this->fetcher->endpoint, $headers);
        }else{
            $request = new Request('GET', $endpoint, $headers);
        }


        $promise = $this->client->sendAsync($request);

        $response = $promise->wait();

        $result = (array)\GuzzleHttp\json_decode($response->getBody());

        return [
            array_merge($this->retrievedData, $result[$this->fetcher->all_results_field]),
            $result[$this->fetcher->next_link]
        ];
    }
    #endregion

}