<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/7/2018
 * Time: 2:07 PM
 */

namespace App\Services\ApiFetchers\Fetchers;


use App\Fetcher;
use App\Services\Facades\CacheRec;
use App\Services\Facades\LogRec;

final class OneStepFetcher extends ApiFetcher
{

    public function __construct(Fetcher $fetcher)
    {
        parent::__construct($fetcher);
    }

    #region MAIN METHODS
    public function handle()
    {
        for($requestQty = 0; $requestQty < $this->fetcherRequestLimit; $requestQty++)
        {
            if($this->requestQtyLimitsReached()){
                LogRec::debug([
                    'process'=>'fetching data from remote api',
                    'message'=>"requests limit exceeded for {$this->fetcher->name}"
                ]);
                break;
            }

            $this->incrementRequestQty();

            if($requestQty === 0){
                [$this->retrievedData, $this->nextPageLink] = $this->getFirstRequestResults();
            }else{
                if($this->nextPageLink){
                    [$this->retrievedData, $this->nextPageLink] = $this->getNextRequestResults($this->nextPageLink);
                }
            }

            if(!$this->nextPageLink){
                LogRec::debug([
                    'process'=>"fetching data from remote api",
                    'message'=>"pagination for fetcher {$this->fetcher->name} reached end"
                ]);
                break;
            }
        }
        $cleanResults = $this->prepareRetrievedDataForCacheRecording();
        CacheRec::savePlayers($cleanResults, $this->fetcher);
    }
    #endregion

    #region SERVICE METHODS
    private function prepareRetrievedDataForCacheRecording()
    {
        $cleanResults = [];

        foreach ($this->retrievedData AS $key=>$entity){
            for($a=1; $a<5; $a++){
                $field = 'field_'.$a;
                if($this->fetcher->$field && array_key_exists($this->fetcher->$field, $entity)){
                    $entityField = $this->fetcher->$field;
                    $cleanResults[$key][$a.'_'.$this->fetcher->$field] = $entity->$entityField;
                }
            }
            // adding entity url
            $entityEndpoint = $this->fetcher->entity_url;
            if($entityEndpoint){
                $cleanResults[$key]['5_entity_url'] = $entity->$entityEndpoint;
            };
            // adding image url
            $entityImageUrl = $this->fetcher->image_url;
            if($entityImageUrl){
                $cleanResults[$key]['6_image_url'] = $entity->$entityImageUrl;
            };
        }
        return $cleanResults;
    }
    #endregion
}