<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/7/2018
 * Time: 12:39 PM
 */

namespace App\Services\ApiFetchers;

use App\Fetcher;
use App\Services\Facades\LogRec;

class ApiFetcherDynamicProvider
{
    public function make(Fetcher $fetcher)
    {
        try{
            $fetcherClass = $fetcher->fetcher_type->fetcher_class;
        }catch(\Exception $exception){
            LogRec::error([
                'process'=>'retrieving class name from fetchers_types',
                'error'=>$exception->getMessage()
            ]);
        }

        $fetcherClass = "App\\Services\\ApiFetchers\\Fetchers\\".$fetcherClass;

        if (! class_exists($fetcherClass)) {
            LogRec::error([
                'process'=>'checking class availability for fetching data from remote api',
                'error'=>"class {$fetcherClass} for fetching api data doesn't exist"
            ]);
            exit();
        }
        return new $fetcherClass($fetcher);
    }
}