<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/7/2018
 * Time: 6:52 PM
 */

namespace App\Services\Facades;

use Illuminate\Support\Facades\Facade;

class CacheRec extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'CacheRec';
    }
}