<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/7/2018
 * Time: 12:36 PM
 */

namespace App\Services\Facades;

use App\Services\ApiFetchers\ApiFetcherDynamicProvider;
use Illuminate\Support\Facades\Facade;

class ApiFetcher extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ApiFetcherDynamicProvider::class;
    }
}