<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/8/2018
 * Time: 3:07 PM
 */

namespace App\Services\Facades;

use Illuminate\Support\Facades\Facade;

class MatchPlayer extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'MatchPlayer';
    }
}