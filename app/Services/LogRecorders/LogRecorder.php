<?php
/**
 * Created by PhpStorm.
 * User: Andrey Naumoff
 * Date: 3/21/2018
 * Time: 5:38 PM
 */

namespace App\Services\LogRecorders;


interface LogRecorder
{
    public function error($message);

    public function debug($message);
}