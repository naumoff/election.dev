<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/7/2018
 * Time: 6:51 PM
 */

namespace App\Services\CacheRecorders;

use App\Fetcher;
use Illuminate\Support\Facades\Redis;
use ErrorException;
use Illuminate\Support\Facades\Cache;

class RedisCacheRecorder implements CacheRecorder
{
    #region MAIN METHODS
    public function savePlayers(array $cleanData, Fetcher $fetcher)
    {
        Redis::pipeline(function ($pipe) use ($cleanData, $fetcher) {
            foreach ($cleanData AS $key=>$entity){
                $pipe->hmset("fetchers:{$fetcher->name}:{$key}", $entity);
            }
        });
    }

    public function getPlayers()
    {
        $keys = Redis::keys('fetchers:*');

        $players = [];
        foreach ($keys AS $key){
            $players[$key] = Redis::hgetall($key);
        }

        return $players;
    }

    public function getPlayersByKeys(array $playersKeys)
    {
        $players = [];
        foreach ($playersKeys AS $playersKey){
            $players[$playersKey] = Redis::hgetall($playersKey);
        }
        foreach ($players AS $playerKey=>$playerArray){
            if(count($playerArray) < 6){
                $players[$playerKey]['6_image_url'] = asset('storage/noimage.svg');
            }
        }
        return $players;
    }

    public function getPlayersKeys()
    {
        return Redis::keys('fetchers:*');
    }

    public function saveMatches(array $matchesList)
    {
        Redis::pipeline(function ($pipe) use ($matchesList) {
            foreach ($matchesList AS $key=>$match){
                $pipe->sadd("matches:{$key}", implode(' ', array_keys($match)));
            }
        });
    }

    public function getMatchesList()
    {
        return Redis::keys('matches:*');
    }

    public function saveScores(array $scoresList)
    {
        Redis::pipeline(function ($pipe) use ($scoresList) {
            foreach ($scoresList AS $key=>$score){
                $pipe->set("scores:{$key}", $score);
            }
        });
    }

    public function getScores()
    {
        $scoreKeys = Redis::keys('scores:*');
        $scores = [];

        foreach ($scoreKeys AS $scoreKey){
            $scores[$scoreKey] = Redis::get($scoreKey);
        }

        return $scores;
    }

    public function getScoreByKey($playerKey)
    {
        $playerScore = Redis::get('scores:'.$playerKey);
        return $playerScore;
    }

    public function getMatchPlayersAndDeleteMatch()
    {
        try{
            $matchKey = Redis::keys('matches:*')[0];
        }catch (ErrorException $exception){
            return false;
        }


        $playersArray = explode(' ', Redis::smembers($matchKey)[0]);

        Redis::del($matchKey);

        return $playersArray;
    }

    public function incrementScore($playerKey)
    {
        Redis::incr('scores:'.$playerKey);
    }

    public function decrementScore($playerKey)
    {
        Redis::decr('scores:'.$playerKey);
    }
    #endregion

    #region SERVICE METHODS

    #endregion

}