<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/7/2018
 * Time: 6:50 PM
 */

namespace App\Services\CacheRecorders;


use App\Fetcher;

interface CacheRecorder
{
    public function savePlayers(array  $cleanData, Fetcher $fetcher);

    public function getPlayers();

    public function getPlayersByKeys(array $playersKeys);

    public function getPlayersKeys();

    public function saveMatches(array $matchesList);

    public function getMatchesList();

    public function saveScores(array $scoresList);

    public function getScores();

    public function getScoreByKey($playerKey);

    public function getMatchPlayersAndDeleteMatch();

    public function incrementScore($playerKey);

    public function decrementScore($playerKey);
}