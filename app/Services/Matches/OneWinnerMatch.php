<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/8/2018
 * Time: 3:04 PM
 */

namespace App\Services\Matches;


use App\Champion;
use App\Services\Facades\CacheRec;
use App\Services\Traits\Fetcher;
use Carbon\Carbon;

class OneWinnerMatch extends Match
{
    use Fetcher;

    private $players;

    #region MAIN METHODS
    public function formMatchesList()
    {
        $this->players = CacheRec::getPlayers();

        $matches = [];
        foreach ($this->players AS $key=>$player){
            $matches = array_merge($matches, $this->generateMatchesForOnePlayer($key));
            unset($this->players[$key]);
        }
        shuffle($matches);
        CacheRec::saveMatches($matches);
    }

    public function countMatches()
    {
        $matchesList = CacheRec::getMatchesList();

        return count($matchesList);
    }

    public function formScoresList()
    {
        $keys = CacheRec::getPlayersKeys();
        $scores = [];
        foreach ($keys AS $key){
            $scores[$key] = 0;
        }

        CacheRec::saveScores($scores);
    }

    public function startMatchAndGetPlayersKeys()
    {
        $playersArray =  CacheRec::getMatchPlayersAndDeleteMatch();

        return $playersArray;
    }

    public function won($playerKey)
    {
        CacheRec::incrementScore($playerKey);
    }

    public function lost($playerKey)
    {
        CacheRec::decrementScore($playerKey);
    }

    public function saveChampions()
    {
        $scores = CacheRec::getScores();
        if(!$scores){
            return false;
        }

        $championsKeys = array_keys($scores, max($scores));

        $playersKeys = [];
        foreach ($championsKeys AS $key=>$championsKey){
            $playersKeys[] = str_replace('scores:','', $championsKey);
        }

        $highScore = max($scores);

        $champions = CacheRec::getPlayersByKeys($playersKeys);

        $data = $this->prepareChampionsDataForDBInsertion($champions, $highScore);

        Champion::insert($data);

        return true;
    }
    #endregion

    #region SERVICE METHODS
    private function generateMatchesForOnePLayer($playerKey)
    {
        $matches = [];
        foreach ($this->players AS $key=>$player){
            if($playerKey !== $key){
                $matches[] = [
                    $playerKey=>$this->players[$playerKey],
                    $key=>$this->players[$key]
                ];
            }
        }
        return $matches;
    }

    private function prepareChampionsDataForDBInsertion($champions, $highScore): array
    {
        $data = [];
        $countOne = 0;
        foreach ($champions AS $key => $champion) {
            $data[$countOne]['fetcher'] = $this->getFetcherName($key);
            $countTwo = 1;
            foreach ($champion AS $fieldName => $fieldValue) {
                if ($countTwo <= 4) {
                    $data[$countOne]['field_' . $countTwo] = json_encode([$fieldName => $fieldValue]);
                } elseif ($countTwo == 5) {
                    $data[$countOne]['score'] = $highScore;
                    $data[$countOne]['remote_link'] = $fieldValue;
                    $data[$countOne]['created_at'] = Carbon::now();
                    $data[$countOne]['updated_at'] = Carbon::now();
                } elseif ($countTwo === 6) {
                    $data[$countOne]['image_url'] = $fieldValue;
                }
                $countTwo++;
            }
            $countOne++;
        }
        return $data;
    }
    #endregion
}