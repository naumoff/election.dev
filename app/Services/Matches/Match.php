<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/8/2018
 * Time: 3:03 PM
 */

namespace App\Services\Matches;


abstract class Match
{
    abstract public function formMatchesList();

    abstract public function countMatches();

    abstract public function formScoresList();

    abstract public function startMatchAndGetPlayersKeys();

    abstract public function won($playerKey);

    abstract public function lost($playerKey);

    abstract public function saveChampions();
}