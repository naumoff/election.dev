<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/9/2018
 * Time: 11:22 AM
 */

namespace App\Services\Traits;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

trait ArrayPaginator
{
    /**
     * @param Request $request
     * @param $scoreResults
     * @return LengthAwarePaginator
     */
    protected function paginateArray(Request $request, $scoreResults, $itemsPerPage): LengthAwarePaginator
    {
        // Get current page form url e.x. &amp;page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Create a new Laravel collection from the array data
        $itemCollection = collect($scoreResults);

        // Define how many items we want to be visible in each page
        $perPage = $itemsPerPage;

        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        // Create our paginator and pass it to the view
        $paginatedItems = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);

        // set url path for generated links
        $paginatedItems->setPath($request->url());

        return $paginatedItems;
    }
}