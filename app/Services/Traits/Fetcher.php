<?php
/**
 * Author: Andrey Naumoff
 * Email: andrey.naumoff@gmail.com
 * Mobile: +380952680707 (viber, telegram, whatsapp)
 * Skype: jq-agent
 * Date: 4/9/2018
 * Time: 10:37 AM
 */

namespace App\Services\Traits;


trait Fetcher
{
    protected function getFetcherName($fetcherKey)
    {
        $pattern = '/:.+:/iU';
        preg_match($pattern, $fetcherKey, $matches);
        $fetcherName = str_replace(':','',$matches[0]);
        return $fetcherName;
    }
}