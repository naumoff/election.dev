<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    #region RELATION METHODS
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    #endregion

    #region MUTATORS METHODS
    public function getIsAdminAttribute()
    {
        return ($this->role_id === 1);
    }

    public function getIsVisitorAttribute()
    {
        return ($this->role_id === 2);
    }
    #endregion
}
