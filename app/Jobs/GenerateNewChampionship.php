<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Facades\MatchPlayer;
use App\Services\Facades\ApiFetcher;
use Illuminate\Support\Facades\Cache;

class GenerateNewChampionship implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(){}

    public function handle()
    {
        //flush scores, matches, fetchers
        Cache::flush();

        $fetchers = \App\Fetcher::all();
        foreach ($fetchers AS $fetcher){
            ApiFetcher::make($fetcher)->handle();
        }

        MatchPlayer::formMatchesList();
        MatchPlayer::formScoresList();
    }
}
