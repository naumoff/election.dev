<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Champion extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'champions';
    protected $fillable = ['*'];
    #endregion

    #region MAIN METHODS
    public function getField1Attribute($value)
    {
        $value = array_values(json_decode($value, true));
        return $value[0];
    }

    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion
}
