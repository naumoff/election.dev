<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFetcherPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'api_id'=>'required|exists:apis,id',
            'fetcher_type_id'=>'required|exists:fetcher_types,id',
            'name'=>'required|unique:fetchers,name',
            'endpoint'=>'required',
            'request_limit'=>'required|numeric|min:1',
            'next_link'=>'required',
            'all_results_field'=>'required',
            'field_1'=>'required',
            'field_2'=>'required',
            'field_3'=>'required',
            'field_4'=>'required',
        ];
    }
}
