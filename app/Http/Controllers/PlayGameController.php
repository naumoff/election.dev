<?php

namespace App\Http\Controllers;

use App\Champion;
use App\Services\Facades\CacheRec;
use App\Services\Traits\Fetcher;
use Illuminate\Http\Request;
use App\Services\Facades\MatchPlayer;
use App\Services\Traits\ArrayPaginator;
use Illuminate\Support\Facades\Cache;

class PlayGameController extends Controller
{
    use Fetcher, ArrayPaginator;

    #region MAIN METHODS
    public function play(){

        $playersKeys = MatchPlayer::startMatchAndGetPlayersKeys();

        //if all matches ended - new champion/champions determined
        if(!$playersKeys){
            if(MatchPlayer::saveChampions() === false){
                return view('pages.endplay');
            };
            Cache::flush();
            return redirect()->route('champions');
        }

        $players = CacheRec::getPlayersByKeys($playersKeys);

        return view('pages.play')->with([
            'players'=> $players,
            'score1' => CacheRec::getScoreByKey($playersKeys[0]),
            'score2' => CacheRec::getScoreByKey($playersKeys[1]),
            'playerOneKey'=>$playersKeys[0],
            'playerTwoKey'=>$playersKeys[1]
        ]);
    }

    public function endgame(Request $request)
    {
        $winner = $request->input('winner');
        $looser = $request->input('looser');

        MatchPlayer::won($winner);
        MatchPlayer::lost($looser);

        return back();
    }

    public function score(Request $request)
    {
        $scores = CacheRec::getScores();
        arsort($scores, SORT_NUMERIC);

        $scoreResults = $this->getScoreResults($scores);

        $paginatedItems = $this->paginateArray($request, $scoreResults,10);

        return view('pages.score', ['scoreResults' => $paginatedItems]);
    }

    public function champions()
    {
        $champions = Champion::orderBy('created_at','desc')->paginate(10);
        return view('pages.champions')->with(['champions'=>$champions]);
    }
    #endregion

    #region SERVICE METHODS
    private function getScoreResults($scores): array
    {
        $scoreResults = [];
        foreach ($scores AS $playerKey => $playerScore) {

            [$playerCleanKey, $playerType] = $this->getPlayerKeyAndType($playerKey, 'scores:');

            $player = CacheRec::getPlayersByKeys([$playerCleanKey]);

            $scoreResults[$playerCleanKey]['type'] = $playerType;

            $count = 1;
            foreach ($player[$playerCleanKey] AS $field => $value) {
                if ($count <= 4) {
                    $scoreResults[$playerCleanKey]['column_' . $count] = $field . ' : ' . $value;
                }
                $count++;
            }
            $scoreResults[$playerCleanKey]['score'] = $playerScore;
        }
        return $scoreResults;
    }

    private function getPlayerKeyAndType($playerKey, $sparePart)
    {
        $playerKey = str_replace($sparePart, '', $playerKey);

        $playerType = $this->getFetcherName($playerKey);

        return [$playerKey,$playerType];
    }
    #endregion
}
