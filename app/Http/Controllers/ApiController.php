<?php

namespace App\Http\Controllers;

use App\Api;
use App\Http\Requests\StoreApiPost;
use App\Http\Requests\UpdateApiPatch;

class ApiController extends Controller
{
    #region RESOURCE METHODS
    public function index()
    {
        $apis = Api::all();
        return view('admin.apis.apis')->with([
            'apis'=>$apis
        ]);
    }

    public function destroy($id)
    {
        $api = Api::find($id);
        $api->delete();
        echo "yes";
    }

    public function store(StoreApiPost $request)
    {
        Api::create($request->only(['base_uri','request_limit']));

        session()->flash('message','New API saved successfully to DB!');
        return redirect()->back();
    }

    public function update(UpdateApiPatch $request, $id)
    {
        $api = Api::find($id);
        $api->update($request->only(['base_uri','request_limit']));
        session()->flash('message','Existing API was successfully updated!');
        return redirect()->back();
    }
    #endregion
}
