<?php

namespace App\Http\Controllers;

use App\Jobs\GenerateNewChampionship;
use App\Services\Facades\MatchPlayer;
use Illuminate\Http\Request;

class SetGameController extends Controller
{
    public function startChampionship()
    {
        $matchesLeft = MatchPlayer::countMatches();

        return view('admin.championship')->with([
            'matchesLeft' => $matchesLeft
        ]);
    }

    public function generateChampionship(Request $request)
    {
        $this->dispatch(new GenerateNewChampionship());
        echo "Task sent to queue. Please run 'artisan queue:work' to start job.";
    }
}
