<?php

namespace App\Http\Controllers;

use App\Api;
use App\Fetcher;
use App\FetcherType;
use App\Http\Requests\StoreFetcherPost;

class FetcherController extends Controller
{
    #region RESOURCE METHODS
    public function index()
    {
        $fetchers = Fetcher::orderBy('api_id')
            ->with(['api','fetcher_type'])
            ->get();

        return view('admin.fetchers.fetchers')->with([
            'fetchers'=>$fetchers
        ]);
    }

    public function destroy($id)
    {
        $fetcher = Fetcher::find($id);
        $fetcher->delete();
        echo "yes";
    }

    public function create()
    {
        $apis = Api::all();
        $fetcherTypes = FetcherType::all();
        return view('admin.fetchers.fetcher-create')->with([
            'apis'=>$apis,
            'fetcherTypes'=>$fetcherTypes
        ]);
    }


    public function store(StoreFetcherPost $request)
    {
        Fetcher::create($request->only([
            'api_id','fetcher_type_id', 'name','endpoint','request_limit','next_link',
            'all_results_field','all_results_subfield','field_1','field_2','field_3','field_4',
            'image_url','entity_url'
        ]));

        session()->flash('message','New Fetcher saved successfully to DB!');
        return redirect()->back();
    }

    public function show($id)
    {
        $fetcher = Fetcher::with(['api','fetcher_type'])->find($id);

        return view('admin.fetchers.fetcher-show')->with([
            'fetcher' => $fetcher
        ]);
    }
    #endregion

    #region MAIN METHODS

    #endregion
}
