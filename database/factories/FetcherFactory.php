<?php

use Faker\Generator as Faker;
use App\Api;
use App\FetcherType;

$factory->define(Api::class, function (Faker $faker) {
    return [
        'base_uri'=>'http://'.$faker->domainName(),
        'request_limit'=>1
    ];
});

$factory->define(FetcherType::class, function(Faker $faker){
    return [
        'name'=>$faker->word(),
        'description'=>$faker->sentence(),
        'fetcher_class'=>ucfirst($faker->word()).ucfirst($faker->word())
    ];
});

$factory->define(\App\Fetcher::class, function(Faker $faker){
    return [
        'api_id'=> factory(Api::class)->create()->id,
        'fetcher_type_id'=>factory(FetcherType::class)->create()->id,
        'name'=>$faker->word,
        'endpoint'=>$faker->word().'/'.$faker->word(),
        'request_limit'=>rand(0,15),
        'queries'=>null, //@todo finish later
        'next_link'=>null, //@todo finish later
        'all_results_field'=>null,
        'field_1'=>'',
        'field_2'=>'',
        'field_3'=>'',
        'field_4'=>'',
        'image_url'=>'',
        'entity_url'=>'',
    ];
});
