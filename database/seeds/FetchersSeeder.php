<?php

use Illuminate\Database\Seeder;

class FetchersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         factory(\App\ApiFetcher::class)->create();

        /**
         * adding http://swapi.co/ fetcher
         */
        $this->addSwapiData();

        /**
         * adding http://pokeapi.co/ fetcher
         */
        $this->addPokeapiData();
    }

    #region SERVICE METHODS
    private function addSwapiData(): void
    {
        $api = factory(\App\Api::class)->create([
            'base_uri' => 'https://swapi.co/api/',
            'request_limit' => '30'
        ]);

        $fetcherType = factory(\App\FetcherType::class)->create([
            'name' => 'swapi',
            'description' => 'One Step ApiFetcher - this class fetches all required entities using only one request',
            'fetcher_class' => 'OneStepFetcher'
        ]);

        $people = \App\Fetcher::create([
            'api_id' => $api->id,
            'fetcher_type_id' => $fetcherType->id,
            'name' => 'people',
            'endpoint' => 'people',
            'request_limit' => 1,
            'headers' => null,
            'queries' => null,
            'next_link' => 'next',
            'all_results_field' => 'results',
            'field_1' => 'name',
            'field_2' => 'height',
            'field_3' => 'skin_color',
            'field_4' => 'birth_year',
            'image_url' => '',
            'entity_url' => 'url',
        ]);

        $species = \App\Fetcher::create([
            'api_id' => $api->id,
            'fetcher_type_id' => $fetcherType->id,
            'name' => 'species',
            'endpoint' => 'species',
            'request_limit' => 1,
            'headers' => null,
            'queries' => null,
            'next_link' => 'next',
            'all_results_field' => 'results',
            'field_1' => 'name',
            'field_2' => 'classification',
            'field_3' => 'average_height',
            'field_4' => 'average_lifespan',
            'image_url' => '',
            'entity_url' => 'url',
        ]);
    }

    private function addPokeapiData()
    {
        $api = factory(\App\Api::class)->create([
            'base_uri' => 'https://pokeapi.co/api/v2/',
            'request_limit' => 30
        ]);

        $fetcherType = factory(\App\FetcherType::class)->create([
            'name' => 'pokeapi',
            'description' => 'Two Step ApiFetcher - this class fetches required entity using 2 requests',
            'fetcher_class' => 'TwoStepFetcher'
        ]);

         $pokemons = \App\Fetcher::create([
            'api_id' => $api->id,
            'fetcher_type_id' => $fetcherType->id,
            'name' => 'pokemons',
            'endpoint' => 'pokemon',
            'request_limit' => 3, //pokeapi.co is too much time consuming (6 sec to get one pokemon)!!!!
            'headers' => null,
            'queries' => null,
            'next_link' => 'next',
            'all_results_field' => 'results',
            'all_results_subfield' => 'url',
            'field_1' => 'name',
            'field_2' => 'weight',
            'field_3' => 'height',
            'field_4' => 'base_experience',
            'image_url' => '',
            'entity_url' => '',
        ]);
    }
    #endregion
}
