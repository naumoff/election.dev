<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 1)->create([
            'name'=>'Andrey Naumoff',
            'email'=>'admin@gmail.com',
            'role_id'=>1,
            'password'=>\Illuminate\Support\Facades\Hash::make('admin')
        ]);

        factory(\App\User::class, 1)->create([
            'name'=>'Simple Visitor',
            'email'=>'visitor@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('visitor')
        ]);
    }
}
