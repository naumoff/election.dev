<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * adding precise user
         * login: admin@gmail.com
         * pass: admin
         */
        $this->call(UsersSeeder::class);

        /**
         * adding fetchers for:
         * http://swapi.co/
         * https://pokeapi.co/
         */
        $this->call(FetchersSeeder::class);
    }
}
