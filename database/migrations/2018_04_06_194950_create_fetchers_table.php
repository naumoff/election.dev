<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFetchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fetchers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('api_id')->unsigned()->index();
            $table->integer('fetcher_type_id')->unsigned()->index();
            $table->string('name')->unique();
            $table->string('endpoint');
            $table->integer('request_limit')->unsigned();
            $table->string('headers')->nullable(); //@todo finish later
            $table->string('queries')->nullable(); //@todo finish later
            $table->string('next_link')->nullable()
                ->comment('link to next page');
            $table->string('all_results_field')->nullable()
                ->comment('field used only by one-step-fetcher(all entities)/two-step-fetcher(link to entity)');
            $table->string('all_results_subfield')->nullable()
                ->comment('field used only by two-step-fetcher to get access to entity url');
            $table->string('field_1')->nullable();
            $table->string('field_2')->nullable();
            $table->string('field_3')->nullable();
            $table->string('field_4')->nullable();
            $table->string('image_url')->nullable();
            $table->string('entity_url')->nullable();
            $table->timestamps();
            $table->foreign('api_id')->references('id')->on('apis')->onDelete('cascade');
            $table->foreign('fetcher_type_id')->references('id')->on('fetcher_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fetchers');
    }
}
